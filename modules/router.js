var url = require("url"),
	fs = require("fs"),	
	LoginController = require("../Controllers/LoginController"),
	TopicController = require("../Controllers/TopicController");

	function parseUrlGenResponse(req, res){
		var pathname = url.parse(req.url).pathname;
		console.log("Pathname: " + pathname);
		var rsc = new RegExp("/resources/(\\w/)*(\\w)+.{1}(\\w)+");
		var idx = new RegExp("/index.html");
		if (rsc.test(pathname)) {
			returnResource(pathname, res);
		} else if (pathname == "/favicon.ico") {
			returnResource('../resources/images/favicon.ico', res);
		} else if (idx.test(pathname) || pathname == "/") {
			returnIndex(req, res);
		} else if (pathname == "/login") {
			LoginController.doLogin(req, res);
		} else if (pathname == '/getTopics'){
			TopicController.getAllTopics(req, res);
		} else if (pathname == '/startTest') {
			TopicController.startTest(req, res);
		} else if (pathname == '/getVars') {
			TopicController.getVars(req, res);
		} else if (pathname == '/saveMark') {
			TopicController.setUserMark(req, res);
		}
	}

	function returnResource(pathname, res){
		pathname = pathname.substring(1);
		fs.readFile(pathname, function(err, data){
			try {
				res.write(data);
				} catch (ex) {
					console.log("Error while getting " + pathname + "\n" + ex);
				};
			res.end();
		});
	}

	function returnIndex(req, res){
		fs.readFile("index.html", function(err, data){
			res.writeHead(200, {
				'Content-Type': 'text/html','Content-Length':data.length
			});
			res.write(data);
			res.end();
		});
	}


exports.route = parseUrlGenResponse;
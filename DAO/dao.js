//it would be better to make a function in each dao, which gets database object and return entity
//instead of db object as now

var sql = require("mysql"),
	dbprops = require("./../Props/dbprops"),
		fcs = require("./../Props/functions"),
			ext = require("node-extjs4"),
				settings = require("./../Props/settings");
		
function createConnection(){
	var connect = sql.createConnection(dbprops.props);
	return connect;
}

Ext.define('BaseDAO', {
	insert: function(entity){
		throw 'Not implemented!';
	},

	select: function(entity){
		throw 'Not implemented!';
	},
	
	updateTable: function(entity){
		throw 'Not implemented!';
	},
	
	del: function(entity){
		throw 'Not implemented!';
	},

	create: function(entity, fn) {
		var client = createConnection();
		var query = this.insert(entity);
		client.query(query,	function(err, info){
			fcs.LOG('EXEC QUERY ' + query);
			//if (err) throw err;			
			fn(info.insertId);
			client.end();
		});
	},

	read: function(entity, fn){
		var client = createConnection();
		var query = this.select(entity);
		client.query(query,	function(err, results, fields){
			fcs.LOG('EXEC QUERY ' + query);
			fn(err, results, fields);
			client.end();
		});
	},

	update: function(entity){
		var client = createConnection();
		var query = this.updateTable(entity);
		client.query(query, function(err){
			fcs.LOG('EXEC QUERY ' + query);
			//if (err) throw err;			
			client.end();
		});
	},
	
	remove: function(entity){
		var client = createConnection();
		var query = this.del(entity);
		client.query(query, function(err){
			fcs.LOG('EXEC QUERY ' + query);
			//if (err) throw err;			
			client.end();
		});
	}
});
		
Ext.define('TopicDAO', {
	extend: 'BaseDAO',
	
	insert: function(entity){
		return "INSERT INTO TOPIC VALUES(0, '" + entity.getTopicName() + "');";
	},

	select: function(entity){
		return "SELECT * FROM TOPIC WHERE TOPIC_ID = " + entity.getTopicId() + ";";
	},
	
	updateTable: function(entity){
		return 'UPDATE TOPIC ' + 'SET NAME = ' + entity.getName() +
				' WHERE TOPIC_ID=' + entity.getTopicId() + ';';
	},
	
	del: function(entity){
		return 'DELETE FROM TOPIC WHERE TOPIC_ID = ' +
							entity.getTopicId() + ';';
	},
	
	getAllTopics: function(fn){
		var client = createConnection();
		var query = 'SELECT * FROM TOPIC;';
		client.query(query,	function(err, results, fields){
			fcs.LOG('EXEC QUERY ' + query);
			fn(err, results, fields);
			client.end();
		});
	}
	
});

Ext.define('UserTopicDAO', {
	extend: 'BaseDAO',
	
	updateTable: function(entity){
		return 'UPDATE USER_TOPIC '
		+ 'SET MARK = ' + entity.getMark()
			+ ', IS_ADMIN = ' + entity.getIsAdmin()
				+ 'WHERE USER_ID = ' + entity.getUserId()
					+ ' AND WHERE TOPIC_ID=' + entity.getTopicId()
						+ ';';
	},
	
	insert: function(entity){
		return 'INSERT INTO USER_TOPIC VALUES(' + entity.getUserId() + ', ' + entity.getTopicId()
			+ ', ' + entity.getMark() + ', ' + entity.getIsAdmin() + ');';
	},
	
	select: function(entity){
		return 'SELECT * FROM USER_TOPIC WHERE USER_ID = ' + entity.getUserId()
			+ ' AND TOPIC_ID = ' + entity.getTopicId() + ';';
	},
	
	del: function(entity){
		return 'DELETE FROM USER_TOPIC WHERE USER_ID = ' + entity.getUserId()
			+ ' AND TOPIC_ID = ' + entity.getTopicId() + ';';
	},
	
	getUsersMarks: function(e, fn){
		//means e - is a user
		var client = createConnection();
		var query = 'SELECT TOPIC_ID, MARK FROM USER_TOPIC WHERE USER_ID = ' + e.getUserId() + ';';		
		client.query(query,	function(err, results, fields){
			fcs.LOG('EXEC QUERY ' + query);
			fn(err, results, fields);
			client.end();
		});	
	},
	
	setUserMark: function(userId, topicId, mark, fn) {
		console.log('called utdao.setUserMark');
		var client = createConnection();
		var query = 'INSERT INTO USER_TOPIC VALUES(' //what if already exists???
			+ userId + ', ' + topicId + ', ' + mark + ', 0);'; // what if admin???
		client.query(query, function(err, info) {
			fcs.LOG('EXEC QUERY ' + query);
			fn();
			client.end();
		});
	}
});

Ext.define('UserDAO', {
	extend: 'BaseDAO',
	
	updateTable: function(entity){
		return 'UPDATE USER SET USERNAME = \'' + entity.getUsername() + '\' , FIRSTNAME = \'' + entity.getFirstname() + '\', LASTNAME = \''
			+ '\', PASSWORD = \'' + entity.getPassword() + '\''
				+ 'WHERE USER_ID = ' + entity.getUserId() + ';';
	},
	
	insert: function(entity){
		return "INSERT INTO USER VALUES(0,'" + entity.getUsername() + "' , '" + entity.getFirstname() + "', '" + entity.getLastname() + "', '"
		+ entity.getPassword() + "');";
	},
	
	select: function(entity){
		return 'SELECT * FROM USER WHERE USERNAME = \'' + entity.getUsername() + '\' AND PASSWORD = \''
		+ entity.getPassword() + '\';';
	},
	
	del: function(entity){
		return 'DELETE FROM USER WHERE USER_ID = ' + entity.getUserId() + ';';
	}
});

Ext.define('QuestionDAO',{
	extend: 'BaseDAO',
	
	updateTable: function(e){
		return 'UPDATE QUESTION SET TEXT = \'' + e.getText() + '\', TOPIC_ID = ' + e.getTopicId()
			+ 'WHERE QUESTION_ID = ' + e.getQuestionId() + ';';
	},
	
	insert: function(e){
		return 'INSERT INTO QUESTION VALUES(0, \'' + e.getText() + '\', ' + e.getTopicId() + ');';
	},
	
	select: function(e){
		return 'SELECT * FROM QUESTION WHERE QUESTION_ID = ' + e.getQuestionId() + ';';
	},
	
	del: function(e){
		return 'DELETE FROM QUESTION WHERE QUESTION_ID = ' + e.getQuestionId() + ';';
	},
	
	getRandomQuestions: function(topicId, fn1) {
		var client = createConnection();
		var query = 'SELECT * FROM QUESTION WHERE TOPIC_ID = ' + topicId + ';';
		client.query(query,	function(err, results, fields){
			fcs.LOG('EXEC QUERY ' + query);
			try {
					console.log(results);
					var subset = fcs.subset(results, settings.questionsForTest);
					//console.log(subset);
					fn1(subset);
				} catch (ex) {
					console.log('Exception in getRandomQuestions ' + ex.message);
				}
			client.end();
		});	
	}
	
});

Ext.define('VariantDAO', {
	extend: 'BaseDAO',
	
	updateTable: function(e){
		return 'UPDATE VARIANT SET TEXT = \'' + e.getText() + '\', IS_CORRECT = ' + e.getIsCorrect()
			+ 'WHERE VARIANT_NUM = ' + e.getVariantNum() + ' AND QUESTION_ID = '
				+ e.getQuestionId() + ';';
	},
	
	insert: function(e){
		return 'INSERT INTO VARIANT VALUES(' + e.getVariantNum() + ', ' + e.getQuestionId()
			+ ', ' + e.getIsCorrect() + ', \'' + e.getText() + '\');';		
	},
	
	select: function(e){
		return 'SELECT * FROM VARIANT WHERE VARIANT_NUM = ' + e.getVariantNum() + ' AND QUESTION_ID = '
				+ e.getQuestionId() + ';';
	},
	
	del: function(e){
		return 'DELETE FROM VARIANT WHERE VARIANT_NUM = ' + e.getVariantNum() + ' AND QUESTION_ID = '
				+ e.getQuestionId() + ';';
	},
	
	fillQuestions: function(questions, fn) {
		var client = createConnection(), qIds = '', q, ids = [];
		//console.log('Questions: ' + questions);
		for (var i = 0; i < questions.length; i++) {
			q = questions[i].question_id;
			questions[i].variants = new Array();		
			qIds = qIds + q + (i == questions.length - 1 ? '' : ', ');
			ids[i] = q;
		}
		var query = 'SELECT * FROM VARIANT WHERE QUESTION_ID IN (' + qIds + ');';
		client.query(query, function(err, results, fields) {
			fcs.LOG('EXEC QUERY ' + query);
			//console.log(results);
			var variant;
			for (var i = 0; i < results.length; i++) {
				variant = results[i];
				//console.log(variant);
				//console.log(ids.indexOf(variant.question_id) + ', ' + variant.question_id);
				if (ids.indexOf(variant.question_id) != -1)
					questions[ids.indexOf(variant.question_id)].variants.push(variant);
			}
			//console.log(questions);
			client.end();
			fn(questions);
		});
	},
	
	getVariants: function(questionId, fn) {
		var client = createConnection();
		var query = 'SELECT * FROM VARIANT WHERE QUESTION_ID = ' + questionId + ';';
		client.query(query, function (err, results, fields) {
			fcs.LOG('EXEC QUERY ' + query);			
			client.end();
			fn(results);
		});
	}
	
});





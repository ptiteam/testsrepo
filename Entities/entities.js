require("node-extjs4");

Ext.define('UserTopic', {
	config: {userId: 0, topicId: 0, mark: -1, isAdmin: false},
	constructor: function(userId, topicId, mark, isAdmin){
		this.userId = userId;
		this.topicId = topicId;
		this.mark = mark;
		this.isAdmin = isAdmin;
	}
});

Ext.define('Topic', {
	config: {topicId: 0, name: ''},
	constructor: function(topicId, name){
		this.topicId = topicId;
		this.name = name;
	},
	toString: function(){
		return '{topicId: ' + this.topicId + ', name: '
			+ this.name + '}';
	}
});

Ext.define('User', {
	config: {userId: 0, username: '', firstname:'', 
	lastname:'', password: ''},
	constructor: function(a, b, c, d, e){
		this.userId = a;
		this.username = b;
		this.firstname = c;
		this.lastname = d;
		this.password = e;
	}
});

Ext.define('Question', {
	config: {questionId: 0, text: '', topicId: 0, variants: []},
	constructor: function(a, b, c){
		this.questionId = a;
		this.text = b;
		this.topicId = c;
	}
});

Ext.define('Variant', {
	config: {variantNum: 100, questionId: 0, text: '', isCorrect: 100},
	constructor: function(a, b, d, c){
		this.variantNum = a;
		this.questionId = b;
		this.correct = c;
		this.text = d;
	}
});
function LOG(message){	
	console.log(message);
}

Array.prototype.shuffle = function() {
   var i = this.length;
   while (--i) {
      var j = Math.floor(Math.random() * (i + 1));
      var temp = this[i];
      this[i] = this[j];
      this[j] = temp;
   }

   return this; // for convenience, in case we want a reference to the array
};

function getSubset(array, howMany) {
	if (howMany > array.length) {
		throw new Error('Array size is too small!');
	} else if (array == null || array.length == 0 || howMany == 0) {
		return [];
	} else {
		return array.shuffle().slice(0, howMany);
	}
}

exports.LOG = LOG;
exports.subset = getSubset;
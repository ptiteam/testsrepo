SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `tests` DEFAULT CHARACTER SET utf8 ;
USE `tests` ;

-- -----------------------------------------------------
-- Table `tests`.`topic`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tests`.`topic` (
  `topic_id` INT(11) NOT NULL,
  `name` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`topic_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tests`.`question`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tests`.`question` (
  `question_id` INT(11) NOT NULL,
  `text` VARCHAR(500) NULL DEFAULT NULL,
  `topic_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`question_id`),
  INDEX `topic_id` (`topic_id` ASC),
  CONSTRAINT `question_ibfk_1`
    FOREIGN KEY (`topic_id`)
    REFERENCES `tests`.`topic` (`topic_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tests`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tests`.`user` (
  `user_id` INT(11) NOT NULL,
  `username` VARCHAR(20) NULL DEFAULT NULL,
  `firstname` VARCHAR(20) NULL DEFAULT NULL,
  `lastname` VARCHAR(20) NULL DEFAULT NULL,
  `password` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tests`.`user_topic`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tests`.`user_topic` (
  `user_id` INT(11) NULL DEFAULT NULL,
  `topic_id` INT(11) NULL DEFAULT NULL,
  `mark` INT(11) NULL DEFAULT NULL,
  `is_admin` TINYINT(1) NULL DEFAULT NULL,
  INDEX `user_id` (`user_id` ASC),
  INDEX `topic_id` (`topic_id` ASC),
  CONSTRAINT `user_topic_ibfk_1`
    FOREIGN KEY (`user_id`)
    REFERENCES `tests`.`user` (`user_id`),
  CONSTRAINT `user_topic_ibfk_2`
    FOREIGN KEY (`topic_id`)
    REFERENCES `tests`.`topic` (`topic_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tests`.`variant`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tests`.`variant` (
  `variant_num` INT(11) NULL DEFAULT NULL,
  `question_id` INT(11) NULL DEFAULT NULL,
  `is_correct` TINYINT(1) NULL DEFAULT NULL,
  `TEXT` VARCHAR(200) NULL DEFAULT NULL,
  INDEX `question_id` (`question_id` ASC),
  CONSTRAINT `variant_ibfk_1`
    FOREIGN KEY (`question_id`)
    REFERENCES `tests`.`question` (`question_id`),
  CONSTRAINT `variant_ibfk_2`
    FOREIGN KEY (`question_id`)
    REFERENCES `tests`.`question` (`question_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

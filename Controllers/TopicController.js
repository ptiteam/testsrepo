var qs = require("querystring"),
	ext = require("node-extjs4"),
	dao = require("../DAO/dao"),
	entities = require("../Entities/entities");

function getAllTopics(req, res){
	if (req.method == "POST"){
	var reqBody = "";
	req.on("data", function(data){
		reqBody += data;
	});
	req.on("end", function(){
		var vars = qs.parse(reqBody);
		var userId = vars.userId;
		var tdao = Ext.create('TopicDAO');
		tdao.getAllTopics(function(err, results, fields){
			if (err || results == null){
				console.log(err);
				res.writeHead(500);
				res.end();
			} else if (results) {
				var topicsArray = [];
				res.writeHead(200);
				var topic;
				var ids = [];
				for (var i = 0; i < results.length; i++){
					topic = results[i];						
					topicsArray[i] = {topicId: topic.topic_id, topicName: topic.name, mark: ''};
					ids[i] = topic.topic_id;
				}
				var utdao = Ext.create('UserTopicDAO');
				utdao.getUsersMarks(Ext.create('User', userId), function(err, marks, fields){
					var rec, id;
					console.log(topicsArray);
					console.log(ids);
					console.log(marks);
					for (var i = 0; i < marks.length; i++) {
						rec = marks[i];
						id = ids.indexOf(rec.TOPIC_ID);
						topicsArray[id].mark = marks[i].MARK < 1 ? '' : marks[i].MARK;
					}
					console.log(topicsArray);
					res.write(JSON.stringify(topicsArray));
					delete topicsArray;
					res.end();
				});
			}
		});
	});
}
}
	
function startTest(req, res) {
	if (req.method == "POST" || true){
	var reqBody = "";
	req.on("data", function(data){
		reqBody += data;
	});
	
	console.log(reqBody);
	
	req.on("end", function(){
		var vars = qs.parse(reqBody);
		console.log(vars);
		var topicId = vars.topicId;
		
		var qdao = Ext.create('QuestionDAO');
		
		qdao.getRandomQuestions(topicId, function(questions) {
			var  q, i;
			console.log(questions);
			for (i = 0; i < questions.length; i++) {
				q = questions[i];
				//delete q.question_id;
				delete q.topic_id;
//				for (k = 0; k < q.variants.length; k++) {
//					v = q.variants[k];
//					delete v.variant_num;
//					//delete v.question_id;
//				}
			}
			console.log(JSON.stringify(questions));
			res.write(JSON.stringify(questions));
			res.end();
		});
		
	});
}
}

function getVars(req, res) {
	var vdao = Ext.create('VariantDAO');
	
	if (req.method == "POST" || true){
		var reqBody = "";
		req.on("data", function(data){
			reqBody += data;
		});
		
		req.on("end", function(){
			var questionId = qs.parse(reqBody).questionId;
			vdao.getVariants(questionId, function(vars) {
				res.write(JSON.stringify(vars));
				res.end();
			});
		});
	}
}

function setUserMark(req, res) {
	var utdao = Ext.create('UserTopicDAO');
	
	if (req.method == "POST" || true){
		var reqBody = "";
		req.on("data", function(data){
			reqBody += data;
		});
		
		req.on("end", function(){
			var userId = qs.parse(reqBody).userId;
			var topicId = qs.parse(reqBody).topicId;
			var mark = qs.parse(reqBody).mark;
			console.log('calling utdao.setUserMark');
			utdao.setUserMark(userId, topicId, mark, function() {
				res.end();
			});
		});
	}
}

exports.setUserMark = setUserMark;
exports.getAllTopics = getAllTopics;
exports.startTest = startTest;
exports.getVars = getVars;

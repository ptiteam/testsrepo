var qs = require ("querystring"),
	crypto = require("crypto"),
	ext = require("node-extjs4"),
	dao = require("../DAO/dao"),
	entities = require("../Entities/entities");

function doLogin(req, res){
	console.log('exec doLogin');
	if (req.method == "POST") {
		var reqBody = "";
		req.on("data", function(data) {
			reqBody += data;
		});
		req.on("end", function() {
			var vars = qs.parse(reqBody);
			var udao = Ext.create("UserDAO");
			var sha = crypto.createHash("sha1");
			sha.update(vars.password);
			var pwdHash = sha.digest('hex');
			var user = Ext.create("User", 0, vars.login, 'First', 'Last', pwdHash);
			udao.read(user, function(err, results){
				console.log('got results ' + results);
				if (err || results.length == 0) {
					console.log(err);
					res.writeHead(401);
					res.end();
				}else if (results[0]) {
					console.log('User ' + vars.login + ' logged in');
					res.writeHead(200);
					res.write(JSON.stringify({userId: results[0].user_id, text: 'Successfully logged in!'}));
					res.end();
				}
			});
		});
	}
}

function doLogout() {
	//code, removing session
}


exports.doLogout = doLogout;
exports.doLogin = doLogin;
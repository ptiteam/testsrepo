Ext.define('LoginWindow', {
	extend: 'Ext.window.Window',
	id: 'loginWindow',
	bodyPadding: 10,
	resizable: false,
	closable: false,
	title: 'Welcome to on-line testing',
	height: 130,
	width: 290,
	items: [{
			xtype: 'textfield',
			id: 'loginField',
			name: 'name',
			fieldLabel: 'Login name'
		}, {
			xtype: 'textfield',
			id: 'pwdField',
			name: 'password',
			fieldLabel: 'Password',
			inputType:'password'
		},{
			xtype: 'button',
			id: 'yes',
			text: 'Login',
			width: 105,
			listeners: {
				click: function(){
					Ext.Ajax.request({
						url: serverUrl + 'login',
						method: 'POST',						
						params: {
							login: Ext.getCmp('loginField').getValue(), 
							password: Ext.getCmp('pwdField').getValue()
						},
						success: function(res, opts){
							var data = Ext.decode(res.responseText);
							//console.log(data);
							//console.log('Success!', data.text);							
							var credStore = Ext.create('CredentialsStore');
							credStore.add({username: Ext.getCmp('loginField').getValue(), userId: data.userId});
							viewController.showCoursesWindow();
							//console.log('username in store is ' + credStore.last().get('username'));
						},
						failure: function(res, opts){						
							if (res.status == 401) {
								Ext.Msg.alert('Error!..', 'Wrong credentials');
							} else {
								Ext.Msg.alert('Error!..', 'Smth wrong in login process');
							}
						}
					});
				}
			}
		}]
});

Ext.define('CoursesWindow', {
	extend: 'Ext.window.Window',
	id: 'coursesWindow',
	title: 'Available courses',
	resizable: false,
	scrolable: true,
	width: 400,
	items: [{
		xtype: 'label',
		html: '<br>&emsp;Select one of these courses:<br><br>'
	}, {
		xtype: 'grid',
		id: 'coursesGrid',
		columnLines: true,
		border: false,
		columns: [{
			header: Locale.courseId,
			dataIndex: 'topicId',
			width: 97
		}, {
			header: Locale.courseName,
			dataIndex: 'topicName',
			width: 97
		}, {
			header: Locale.yourMark,
			dataIndex: 'mark',
			width: 97
		}, {
			xtype: 'actioncolumn',
			header: 'Pass test',
			tooltip: 'Go through the test',
			width: 97,
			items: [{
				icon: 'resources/images/passTest.ico',
				handler: function(grid, row, col){ //TODO ajax request should be in controller
					var credStore = Ext.getStore('credStore');
					var topicId = grid.getStore().getAt(row).get('topicId');
					console.log('topic = ' + topicId);
					Ext.Ajax.request({ //transfer logic to controller?
						url: serverUrl + 'startTest',
						method: 'POST',						
						params: {
							userId: credStore.last().get('userId'),
							topicId: topicId
						},
						success: function(qs, opts) {
							console.log(qs);
							console.log('viewController' + viewController);
							if (qs.responseText != '')
								viewController.startTest(topicId);
						}
					});
				} // end handler
			}] //end items of act column
		}], // end columns of grid
		store: 'coursesStore'
	}]
});

Ext.define('TestWindow', {
    extend: 'Ext.window.Window',
    
    title: 'Question',
	id: 'testWindow',
	resizable: false,
	scrolable: true,
	height: 200,
	width: 300,
    buttonAlign: 'right',
    topicId: null,
    
    items: [
        {
            xtype: 'label',
			style: 'text-align: center',
            id: 'questionText',
            html: '<br><center>This course doesn\'t have questions yet</center><br>'
        },
        {   xtype      : 'fieldcontainer',
			id: 'varsSet',
            defaultType: 'radio'
        },
        {
            xtype: 'button',
            id: 'answerBtn',
            html: 'Answer',
            handler: function() {
            	Ext.getCmp('nextQuestionBtn').setDisabled(false);
				Ext.getCmp('answerBtn').setDisabled(true);
                var items = Ext.getCmp('varsSet').items.items, chosenItem = null;
				console.log(items);
				for (var i = 0; i < items.length; i++) {
					if (items[i].checked) {
						chosenItem = items[i];
						break;
					}
				}
				if (chosenItem != null && chosenItem.isCorrect) {
					var ansStore = Ext.getStore('correctAnswersStore');
					var answers = ansStore.last().get('correctAnswers');
					ansStore.last().set('correctAnswers', answers + 1);
					//console.log(ansStore.last().get('correctAnswers'));
					//sdasdasds
				}
            }
        },
        {
            xtype: 'button',
            html: 'Next >>',
			id: 'nextQuestionBtn',
			disabled: true
        },
		{
			xtype: 'button',
			html: 'Go to courses view',
			id: 'interruptTest',
			handler: function() {
				viewController.stopTest(Ext.getCmp('testWindow').topicId, true);
			}
		}
    ]
    
});
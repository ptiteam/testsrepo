Locale = {
	yourMark: 'Your mark',
	courseId: 'Course Id',
	courseName: 'Course Name'
}

var serverUrl = document.URL;



var viewController = Ext.define('ViewController', {
	extend: 'Ext.app.Controller',
	id: 'viewController',
	
	showLoginWindow: function (){
		var loginWin = Ext.create('LoginWindow');
		loginWin.show();
	},
	
	showCoursesWindow: function (){
		var credStore = Ext.getStore('credStore');			
		if (credStore) {
			Ext.getCmp('loginWindow').hide();
			var coursesStore = Ext.create('CoursesStore');
			coursesStore.getProxy().url = serverUrl + 'getTopics';
			coursesStore.getProxy().extraParams = { userId: credStore.last().get('userId')};
			coursesStore.load(function(recs){
				Ext.create('CoursesWindow').show();
				Ext.getCmp('coursesGrid').store = coursesStore;
			});
		}
	},
	
	startTest: function(topicId) {		
		var credStore = Ext.getStore('credStore');
		console.log('credStore = ' + credStore);
		var me = this;
		if (credStore) {
			Ext.getCmp('coursesWindow').hide();
			var qsStore = Ext.create('QuestionsStore');
			qsStore.getProxy().url = serverUrl + 'startTest';
			qsStore.getProxy().extraParams = { userId: credStore.last().get('userId'),
							topicId: topicId};
			console.log('qStore extra params ' + qsStore.getProxy().extraParams);
			qsStore.load(function(recs) {
				console.log(recs);
				if (recs) {
					var testWindow = Ext.create('TestWindow');
					testWindow.topicId = topicId;
					testWindow.show();
					var answersStore = Ext.create('CorrectAnswersStore');
					answersStore.loadRawData({correctAnswers: 0}, false);
					me.loadQuestion(recs, 1);
				}
			});
		}
	},
	
	
	loadQuestion: function(qs, index) { //index starts from 1
		var qStore = Ext.getStore('questionsStore');
		var me = this;
		var lastQuestion = index == qStore.getCount();
		if (lastQuestion) {
			Ext.getCmp('nextQuestionBtn').setText('Finish'); //TODO why it doesn't work?
			Ext.getCmp('nextQuestionBtn').handler = function() {
				var ansStore = Ext.getStore('correctAnswersStore');				
				alert('You have finished the test with ' + ansStore.last().get('correctAnswers') + ' correct answers');
				viewController.stopTest(Ext.getCmp('testWindow').topicId, false);
				//ansStore.removeAll();
			};
		} else {
			Ext.getCmp('nextQuestionBtn').handler = function() {
				Ext.getCmp('nextQuestionBtn').setDisabled(true);
				Ext.getCmp('answerBtn').setDisabled(false);
				me.loadQuestion(qs, index + 1);
			};
		}
		Ext.getCmp('questionText').setText(qs[index - 1].data.text + '\n'); //TODO format text to center
		if (qStore.getCount() == 0) { //TODO why it doesn't work?
			console.log('qStore count is 0');
			Ext.getCmp('answerBtn').setDisabled(true);
			return;
		}
		//load variants from server and fill the form
		Ext.Ajax.request({
			url: serverUrl + 'getVars',
			method: 'POST',						
			params: {
				questionId: qs[index - 1].data.question_id
			},
			success: function(vars, opts) {
				vars = Ext.JSON.decode(vars.responseText);
				console.log(vars);
				var varsSet = Ext.getCmp('varsSet');
				varsSet.removeAll();
				varsSet.items.items = [];
				var v, varBox;
				for (var i = 0; i < vars.length; i++) {
					v = vars[i];					
					varBox = Ext.create('Ext.form.field.Radio', {
						id: 'var' + i, 
						isCorrect: v.is_correct, 
						boxLabel: v.TEXT, 
						name: 'variant'
					});					
					varsSet.add(varBox);
				}
			}
		});		
	},
	
	stopTest: function(topicId, inTheMiddle) {
		// if inTheMiddle == true, clear questions store, answers store etc
		// if inTheMiddle == false, send mark to server and clear stores
		var me = this;
		var finishFn = function() {
			Ext.getStore('questionsStore').removeAll();
			Ext.getStore('correctAnswersStore').removeAll();
			Ext.getCmp('coursesWindow').close();
			Ext.getCmp('testWindow').close();
			me.showCoursesWindow();
		};
		if (!inTheMiddle) {
			Ext.Ajax.request({
				url: serverUrl + 'saveMark',
				method: 'POST',
				
				params: {
					userId: Ext.getStore('credStore').last().get('userId'),
					topicId: topicId,
					mark: Ext.getStore('correctAnswersStore').last().get('correctAnswers')
				},
				
				success: function(vars, opts) {					
					finishFn();
				}
				
			});
		} else {
			finishFn();
		}
	}
	
});

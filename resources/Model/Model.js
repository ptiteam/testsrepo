Ext.define('CredentialsStore', {
	extend: 'Ext.data.Store',
	fields: ['username', 'userId'],
	storeId: 'credStore'
});

Ext.define('CoursesStore', {
	extend: 'Ext.data.Store',
	
	fields: ['topicId', 'topicName', 'mark'],
	storeId: 'coursesStore',
	proxy: {
		type: 'ajax',
		url: '',
		actionMethods: {
			read: 'POST'
		},
		reader: {
			type: 'json'				
		}
	}
});

Ext.define('QuestionsStore', {
	extend: 'Ext.data.Store',
	
	fields: ['text', 'variants', 'question_id'],
	storeId: 'questionsStore',
	proxy: {
		type: 'ajax',
		url: '',
		actionMethods: {
			read: 'POST'
		},
		reader: {
			type: 'json'				
		}
	}
});

Ext.define('CorrectAnswersStore', {
	extend: 'Ext.data.Store',
	
	fields: ['correctAnswers'],
	storeId: 'correctAnswersStore',
	
	proxy: {
		type: 'ajax',
		url: '',
		actionMethods: {
			read: 'POST'
		},
		reader: {
			type: 'json'				
		}
	}
});